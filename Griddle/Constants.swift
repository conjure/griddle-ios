//
//  Constants.swift
//  Griddle
//
//  Created by Piotr Wilk on 11/02/2022.
//

import Foundation

struct AdMob {
    static let nextLevelAdID    = "ca-app-pub-6897741366254298/5626488356"
    static let sameLevelAdID    = "ca-app-pub-6897741366254298/3297250593"
    
    //https://developers.google.com/admob/ios/test-ads
    static let adUnitIDTest     = "ca-app-pub-3940256099942544/4411468910"
}
