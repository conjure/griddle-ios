//
//  UIApplication+Extension.swift
//  Griddle
//
//  Created by Piotr Wilk on 14/04/2022.
//

import UIKit

extension UIApplication {
    //https://stackoverflow.com/a/58673530
    var activeWindowScene: UIWindowScene? {
        return UIApplication.shared.connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .first(where: { $0 is UIWindowScene })
            .flatMap({ $0 as? UIWindowScene })
    }
}
