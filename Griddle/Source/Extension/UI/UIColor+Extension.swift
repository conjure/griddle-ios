//
//  UIColor+Extension.swift
//  Gridle
//
//  Created by Piotr Wilk on 04/02/2022.
//

import UIKit

extension UIColor {
    
    struct appPalette {
        static let darkGray = UIColor(hex: 0x2F2F2F)
        static let tileGray = UIColor(hex: 0x535353)
        static let cyan = UIColor(hex: 0x3EFFD9)
        
        static let yellow = UIColor(hex: 0xEBE553)
        static let purple = UIColor(hex: 0xA99AE3)
        static let blue = UIColor(hex: 0x89DADF)
        static let green = UIColor(hex: 0x89DF91)
        static let orange = UIColor(hex: 0xFEB97A)
    }
    
    convenience init(hex: Int) {
        let rgb = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        
        self.init(red: rgb.R, green: rgb.G, blue: rgb.B, alpha: 1)
    }
}
