//
//  UIView+Extension.swift
//  Griddle
//
//  Created by Piotr Wilk on 23/02/2022.
//

import UIKit

extension UIView {
    // https://stackoverflow.com/a/46562641
    func shake(duration: TimeInterval = 0.6, values: [CGFloat]) {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = duration
        animation.values = values
        self.layer.add(animation, forKey: "shake-animation")
    }
    
    func jiggle() {
        let degrees: CGFloat = 10
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        animation.duration = 0.5
        animation.isCumulative = true
        animation.repeatCount = 1//Float.infinity
        animation.values = [
            0.0,
            (-degrees).degreesToRadians * 0.25,
            0.0,
            (degrees).degreesToRadians * 0.5,
            0.0,
            (-degrees).degreesToRadians,
            0.0,
            (degrees).degreesToRadians,
            0.0,
            (-degrees).degreesToRadians * 0.5,
            0.0,
            (degrees).degreesToRadians * 0.25,
            0.0
        ]
        animation.fillMode = .forwards;
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.isRemovedOnCompletion = true
        
        layer.add(animation, forKey: "jiggle-animation")
    }
}

extension FloatingPoint {
    var degreesToRadians: Self { self * .pi / 180 }
    var radiansToDegrees: Self { self * 180 / .pi }
}
