//
//  NSObject+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import Foundation

extension NSObject {
    class var className: String {
        NSStringFromClass(self).components(separatedBy: ".").last!
    }

    var className: String {
        let thisType = type(of: self)
        return String(describing: thisType)
    }
}

func runOnMainThreadAsync(_ completion: @escaping (() -> Void)) {
    DispatchQueue.main.async {
        completion()
    }
}

func runOnMainThreadAsyncAfter(_ seconds: Double, _ completion: @escaping (() -> Void)) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}

func runOnBackgroundThread(_ completion: @escaping (() -> Void)) {
    DispatchQueue.global(qos: .background).async {
        completion()
    }
}
