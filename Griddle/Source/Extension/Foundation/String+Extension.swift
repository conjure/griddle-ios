//
//  String+Extension.swift
//  Gridle
//
//  Created by Piotr Wilk on 03/02/2022.
//

import Foundation

extension StringProtocol {
    subscript(offset: Int) -> Character {
        self[index(startIndex, offsetBy: offset)]
    }
}
