//
//  Array+Extension.swift
//  Griddle
//
//  Created by Piotr Wilk on 22/02/2022.
//

import Foundation

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}
