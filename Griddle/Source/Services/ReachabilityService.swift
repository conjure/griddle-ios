//
//  ReachabilityService.swift
//  Griddle
//
//  Created by Piotr Wilk on 29/04/2022.
//

import Foundation
import Reachability

class ReachabilityService: Reachable {
    private var reachability: Reachability?
    private(set) var isInternet = false
    
    init() {
        reachability = try? Reachability()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: .reachabilityChanged, object: reachability)
        do {
            try reachability?.startNotifier()
        } catch {
            print("[ReachabilityService] Error, could not start reachability notifier")
        }
    }
    
    @objc
    private func reachabilityChanged(_ notification: Notification) {
        guard let reachability = notification.object as? Reachability else { return }
        
        switch reachability.connection {
        case .wifi:
            print("[ReachabilityService] Reachable via WiFi")
            isInternet = true
        case .cellular:
            print("[ReachabilityService] Reachable via Cellular")
            isInternet = true
        default:
            isInternet = false
        }
    }
}
