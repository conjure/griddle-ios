//
//  AdService.swift
//  Griddle
//
//  Created by Piotr Wilk on 11/02/2022.
//

import UIKit
import GoogleMobileAds

enum AdServiceError: Error {
    case noInternet
}

class AdService: NSObject, AdProvidable {
    weak var delegate: AdProvidableDelegate?
    private var interstitial: GADInterstitialAd?
    let reachability: Reachable
    
    init(_ reachability: Reachable) {
        self.reachability = reachability
        super.init()
    }
    
    func requestSameLevelAd(_ hostViewController: UIViewController, completion: @escaping ((Error?) -> Void)) {
        var unitId = AdMob.sameLevelAdID
        #if targetEnvironment(simulator)
        unitId = AdMob.adUnitIDTest
        #endif
        
        requestAd(unitId, hostViewController: hostViewController, completion: completion)
    }
    
    func requestNextLevelAd(_ hostViewController: UIViewController, completion: @escaping ((Error?) -> Void)) {
        var unitId = AdMob.nextLevelAdID
        #if targetEnvironment(simulator)
        unitId = AdMob.adUnitIDTest
        #endif
        
        requestAd(unitId, hostViewController: hostViewController, completion: completion)
    }
    
    private func requestAd(_ unitId: String, hostViewController: UIViewController, completion: @escaping ((Error?) -> Void)) {
        if !reachability.isInternet {
            completion(AdServiceError.noInternet)
            return
        }
        
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID: unitId,
                               request: request,
                               completionHandler: { [self] ad, error in
            if let error = error {
                print("[AdService] Failed to load interstitial ad with error: \(error.localizedDescription)")
                completion(error)
                return
            }
            
            self.interstitial = ad
            self.interstitial?.present(fromRootViewController: hostViewController)
            self.interstitial?.fullScreenContentDelegate = self
            completion(nil)
        }
        )
    }
}

extension AdService: GADFullScreenContentDelegate {
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        delegate?.adServiceDidDismissFullScreenContent(self)
    }
}
