//
//  WordService.swift
//  Griddle
//
//  Created by Piotr Wilk on 08/02/2022.
//

import Foundation
import GameplayKit

class WordService: WordProvidable {
    private var shuffledDictionary = [String]()
    private(set) var words = [String]()
    private(set) var wordsVertical = [String]()
    private let wordLenght: Int = 5
    
    init(_ fileName: String) {
        loadWords(fileName)
    }
    
    private func loadWords(_ fileName: String) {
        let path = Bundle.main.path(forResource: fileName, ofType: nil)
        let text = try? String(contentsOfFile: path!, encoding: .utf8)
        let lines : [String] = text!.components(separatedBy: "\n")
        
        var dictionary = [String]()
        for wordStr in lines {
            if wordStr.count == wordLenght {
                dictionary.append(wordStr)
            }
            
        }
        
        if let shuffled = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: dictionary) as? [String] {
            shuffledDictionary = shuffled
        }
        print("[WordService] shuffeled words: \(shuffledDictionary.count)")
    }
    
    func randomizeWords(_ count: Int) {
        let result = randomWords(shuffledDictionary, count: count)
        let resultVertical = randomizeVertically(result)
        print("[WordService] words: \(result) vertical: \(resultVertical)")
        words = result
        wordsVertical = resultVertical
    }
    
    private func randomWords(_ dictionary: [String], count: Int) -> [String] {
        var result = Set<String>()
        
        while result.count < count {
            result.removeAll()
            for _ in 0..<5 {
                if let shuffled = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: dictionary) as? [String] {
                    if let randomWord = shuffled.randomElement() {
                        result.insert(randomWord.lowercased())
                    }
                }
            }
            print("[WordService] randomizeWords result: \(result)")
        }
        
        return Array(result)
    }
    
    private func randomizeVertically(_ words: [String]) -> [String] {
        var result = [String](repeating: "", count: words.count)
        
        for i in 0..<words.count {
            var resultVertical = [String]()
            for j in 0..<words.count {
                let wordChar = String(words[j][i])
                resultVertical.append(wordChar)
            }
            
            let resultVerticalShuffeled = resultVertical.shuffled()
            for i in 0..<resultVerticalShuffeled.count {
                var resultWord = result[i]
                resultWord += String(resultVerticalShuffeled[i])
                result[i] = resultWord
            }
            print("[WordService] randomizeVerticalLetters collumn: \(i): \(resultVerticalShuffeled)")
        }
        
        let difference = words.difference(from: result)
        if difference.count < words.count * 2 {
            return randomizeVertically(words)
        }
              
        return result
    }
}
