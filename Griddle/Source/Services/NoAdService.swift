//
//  NoAdService.swift
//  Griddle
//
//  Created by Piotr Wilk on 01/07/2022.
//

import UIKit

class NoAdService: AdProvidable {
    var delegate: AdProvidableDelegate?
    
    func requestSameLevelAd(_ hostViewController: UIViewController, completion: @escaping ((Error?) -> Void)) {
        completion(nil)
        delegate?.adServiceDidDismissFullScreenContent(self)
    }
    
    func requestNextLevelAd(_ hostViewController: UIViewController, completion: @escaping ((Error?) -> Void)) {
        completion(nil)
        delegate?.adServiceDidDismissFullScreenContent(self)
    }
}
