//
//  LocalStorage.swift
//  Griddle
//
//  Created by Piotr Wilk on 22/02/2022.
//

import Foundation

enum LocalStorageKey: RawRepresentable, CaseIterable {
    typealias AllCases = [LocalStorageKey]
    static var allCases: [LocalStorageKey] = [.firstAppLaunch]
    
    case firstAppLaunch
    case gamesCompleted
    
    init?(rawValue: String) {
        fatalError()
    }
    
    var rawValue: String {
        switch self {
        case .firstAppLaunch:
            return "firstAppLaunch"
        case .gamesCompleted:
            return "gamesCompleted"
        }
    }
}

class LocalStorage {
    let userDefaults: UserDefaults
    
    init(userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults
    }
    
    func remove(forKey key: LocalStorageKey) {
        userDefaults.removeObject(forKey: key.rawValue)
    }
}

extension LocalStorage: BoolStorable {
    func setBool(_ value: Bool, forKey key: LocalStorageKey) {
        userDefaults.setValue(value, forKey: key.rawValue)
        userDefaults.synchronize()
    }
    
    func bool(forKey key: LocalStorageKey) -> Bool? {
        userDefaults.value(forKey: key.rawValue) as? Bool
    }
}

extension LocalStorage: IntStorable {
    func setInt(_ value: Int, forKey key: LocalStorageKey) {
        userDefaults.setValue(value, forKey: key.rawValue)
        userDefaults.synchronize()
    }
    
    func int(forKey key: LocalStorageKey) -> Int? {
        userDefaults.value(forKey: key.rawValue) as? Int
    }
}
