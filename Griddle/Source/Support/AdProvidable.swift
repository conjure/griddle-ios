//
//  AdProvidable.swift
//  Griddle
//
//  Created by Piotr Wilk on 22/02/2022.
//

import UIKit

protocol AdProvidableDelegate: AnyObject {
    func adServiceDidDismissFullScreenContent(_ service: AdProvidable)
}

protocol AdProvidable: AnyObject {
    var delegate: AdProvidableDelegate? { get set }
    func requestSameLevelAd(_ hostViewController: UIViewController, completion: @escaping ((Error?) -> Void))
    func requestNextLevelAd(_ hostViewController: UIViewController, completion: @escaping ((Error?) -> Void))
}
