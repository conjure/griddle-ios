//
//  Coordinator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

protocol Coordinator: AnyObject {
    var parent: Coordinator? { get set }
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    init(_ navigationController: UINavigationController, dependencies: AppDependable)
    
    func start()
    func add(child: Coordinator)
    func removeFromParent()
}

extension Coordinator {
    public func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() where coordinator === child {
            childCoordinators.remove(at: index)
        }
    }

    public func add(child: Coordinator) {
        child.parent = self
        childCoordinators.append(child)
    }

    public func removeFromParent() {
        parent?.childDidFinish(self)
    }
}
