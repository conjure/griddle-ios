//
//  Reachable.swift
//  Griddle
//
//  Created by Piotr Wilk on 29/04/2022.
//

import Foundation

protocol Reachable: AnyObject {
    var isInternet: Bool { get }
}
