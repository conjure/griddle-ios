//
//  AppDependable.swift
//  BlueWhale
//
//  Created by Piotr Wilk on 14/02/2022.
//

protocol AppDependable: AnyObject {
    var wordService: WordProvidable { get }
    var adService: AdProvidable { get }
    var localStorage: LocalStorage { get }
}
