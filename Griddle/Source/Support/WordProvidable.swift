//
//  WordProvidable.swift
//  Griddle
//
//  Created by Piotr Wilk on 22/02/2022.
//

import Foundation

protocol WordProvidable: AnyObject {
    var words: [String] { get }
    var wordsVertical: [String] { get }
    func randomizeWords(_ count: Int)
}
