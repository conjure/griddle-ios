//
//  Matrix.swift
//  Gridle
//
//  Created by Piotr Wilk on 03/02/2022.
//

import Foundation

struct Matrix<T> {
    let rows: Int
    let columns: Int
    var grid: [T]
    
    init(rows: Int, columns: Int,defaultValue: T) {
        self.rows = rows
        self.columns = columns
        grid = Array(repeating: defaultValue, count: rows * columns)
    }
    
    func indexIsValid(row: Int, column: Int) -> Bool {
        return row >= 0 && row < rows && column >= 0 && column < columns
    }
    
    subscript(row: Int, column: Int) -> T {
        get {
            assert(indexIsValid(row: row, column: column), "[Matrix] Index out of range")
            return grid[(row * columns) + column]
        }
        set {
            assert(indexIsValid(row: row, column: column), "[Matrix] Index out of range")
            grid[(row * columns) + column] = newValue
        }
    }
    
    func debugPrintGrid() -> String {
        var result = "\n"
        
        for i in 0..<rows {
            for j in 0..<columns {
                if let tile = self[i, j] as? Tile {
                    result += tile.text ?? ""
                }
                
                if let str = self[i, j] as? String {
                    result += str
                }
            }
            result += "\n"
        }
        
        return result
    }
}
