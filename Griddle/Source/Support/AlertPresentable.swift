//
//  AlertPresentable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/08/2021.
//

import UIKit

protocol AlertPresentable: AnyObject {
    func showAlert(_ title: String, message: String, completion: ((UIAlertAction) -> Void)?)
}

extension AlertPresentable where Self: UIViewController {
    func showAlert(_ title: String, message: String, completion: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .default, handler: completion)
        alert.addAction(actionOk)

        present(alert, animated: true)
    }
}
