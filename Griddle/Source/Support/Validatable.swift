//
//  Validatable.swift
//  Gridle
//
//  Created by Piotr Wilk on 03/02/2022.
//

import Foundation

protocol Validatable: AnyObject {
    func validate()
}
