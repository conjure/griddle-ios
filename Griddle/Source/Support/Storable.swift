//
//  Storable.swift
//  Griddle
//
//  Created by Piotr Wilk on 22/02/2022.
//

import Foundation

protocol Storable: AnyObject {
    associatedtype StorageKey: CaseIterable
    
    func remove(forKey key: StorageKey)
    func clearAll()
}

extension Storable {
    func clearAll() {
        for key in StorageKey.allCases {
            remove(forKey: key)
        }
    }
}

protocol BoolStorable: Storable {
    func setBool(_ value: Bool, forKey key: StorageKey)
    func bool(forKey key: StorageKey) -> Bool?
}

protocol IntStorable: Storable {
    func setInt(_ value: Int, forKey key: StorageKey)
    func int(forKey key: StorageKey) -> Int?
}
