//
//  Grid5x5.swift
//  Gridle
//
//  Created by Piotr Wilk on 03/02/2022.
//

import UIKit
import AVFoundation

protocol GridDelegate: AnyObject {
    func gridDelegateDidMove(_ grid: Grid5x5, tile: Tile)
    func gridDelegateDidValidate(_ grid: Grid5x5, correctRows: Int)
}

typealias TileIndex = (collumn: Int, row: Int)

class Grid5x5: BaseView {
    private var tiles: Matrix<Tile> = Matrix(rows: 5, columns: 5, defaultValue: Tile())
    private var words = [String]()
    private var wordsReady = [String]()
    private var selectedTile: Tile?
    private var player: AVAudioPlayer?
    private let feedbackGenerator = UINotificationFeedbackGenerator()
    
    weak var delegate: GridDelegate?
    private(set) var correctRows: Int = 0
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        for i in 0..<5 {
            for j in 0..<5 {
                let tile = Tile(frame: CGRect.zero)
                tiles[i, j] = tile
                addSubview(tile)
            }
        }
        
        isExclusiveTouch = true
        
        isUserInteractionEnabled = true
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(onPanRecognizer))
        addGestureRecognizer(panRecognizer)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let tileSize = (UIScreen.main.bounds.width - 50) / 5
        
        for i in 0..<5 {
            for j in 0..<5 {
                let tile = tiles[i, j]
                let frame = CGRect(x: CGFloat(i) * tileSize + 25, y: CGFloat(j) * tileSize, width: tileSize, height: tileSize)
                tile.frame = frame
            }
        }
        
        for i in 0..<5 {
            let sideTile = Tile()
            let posX: CGFloat = -(tileSize - 18)
            let frame = CGRect(x: posX, y: CGFloat(i) * tileSize, width: tileSize, height: tileSize)
            sideTile.frame = frame
            let color = colorForRow(i)
            sideTile.sideColor = color
            sideTile.reset()
            addSubview(sideTile)
        }
        
        for i in 0..<5 {
            let sideTile = Tile()
            let posX: CGFloat = UIScreen.main.bounds.width - 18
            let frame = CGRect(x: posX, y: CGFloat(i) * tileSize, width: tileSize, height: tileSize)
            sideTile.frame = frame
            let color = colorForRow(i)
            sideTile.sideColor = color
            sideTile.reset()
            addSubview(sideTile)
        }
        
        for i in 0..<5 {
            let color = colorForRow(i)
            for j in 0..<5 {
                let tile = tiles[j, i]
                tile.highlightColor = color
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        for i in 0..<5 {
            for j in 0..<5 {
                let tile = tiles[i, j]
                if tile.basePosition == CGPoint.zero {
                    tile.basePosition = tile.center
                }
            }
        }
    }
    
    func load(_ shuffeled: [String], solved: [String]) {
        self.words = solved
        correctRows = 0
        prepareSound()
        reset()
        
        for i in 0..<shuffeled.count {
            for j in 0..<shuffeled.count {
                let char = String(shuffeled[j][i])
                let tile = tiles[i, j]
                tile.value = char
                tile.index = TileIndex(i, j)
            }
        }
    }
    
    @objc
    private func onPanRecognizer(_ gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            let location = gestureRecognizer.location(in: self)
            
            if selectedTile == nil, let tile = tileFor(location, excludedTile: nil), !tile.isLocked {
                selectedTile = tile
            }
            
            if let tile = selectedTile {
                bringSubviewToFront(tile)
                addShadow(selectedTile)
                selectedTile?.center.y = location.y
            }
            
            gestureRecognizer.setTranslation(CGPoint.zero, in: self)
        }
        
        if gestureRecognizer.state == .ended || gestureRecognizer.state == .cancelled {
            let location = gestureRecognizer.location(in: self)
            
            var otherTile: Tile?
            if selectedTile != nil {
                if let tile = tileFor(location, excludedTile: selectedTile), !tile.isLocked {
                    otherTile = tile
                }
            }
            
            // swap
            if let tileSelected = selectedTile, !tileSelected.isLocked, let tileOther = otherTile, tileSelected.index.collumn == tileOther.index.collumn {
                if !tileOther.isLocked {
                    swapTiles(tileSelected, tileB: tileOther)
                } else {
                    tileSelected.snapTo(tileSelected.basePosition)
                }
            }  else {
                if let tile = selectedTile, !tile.isLocked {
                    selectedTile?.snapTo(tile.basePosition)
                }
            }
            
            if otherTile == nil, let tile = selectedTile, tile.isLocked == false {
                selectedTile?.snapTo(tile.basePosition)
            }
            
            // validate
            validateGrid(false, moved: true)
            if selectedTile != nil {
                delegate?.gridDelegateDidMove(self, tile: selectedTile!)
            }
            
            // shadow
            removeShadow(selectedTile)
            
            // haptic
            if let tileSelected = selectedTile, let tileOther = otherTile {
                if !tileSelected.isLocked, !tileOther.isLocked {
                    playSuccess()
                } else {
                    playHapticError()
                }
            }
            if otherTile == nil, selectedTile != nil {
                playHapticError()
            }
            selectedTile = nil
            
            // colors
            updateColors()
        }
    }
    
    private func tileFor(_ location: CGPoint, excludedTile: Tile?) -> Tile? {
        for i in 0..<5 {
            for j in 0..<5 {
                let tile = tiles[i, j]
                if tile.frame.contains(location), tile != excludedTile {
                    return tile
                }
            }
        }
        
        return nil
    }
    
    private func swapTiles(_ tileA: Tile, tileB: Tile) {
        let positionA = tileA.basePosition
        let positionB = tileB.basePosition
        tileA.snapTo(positionB)
        tileA.basePosition = positionB
        tileB.snapTo(positionA)
        tileB.basePosition = positionA
        
        let indexA = tileA.index
        let indexB = tileB.index
        updateIndex(tile: tileA, index: indexB)
        updateIndex(tile: tileB, index: indexA)
    }
    
    private func swapTilesOLD(_ tileA: Tile, indexA: TileIndex, tileB: Tile, indexB: TileIndex) {
        tileA.snapTo(tileB.basePosition)
        tileB.snapTo(tileA.basePosition)
        updateIndex(tile: tileA, index: indexB)
        updateIndex(tile: tileB, index: indexA)
    }
    
    private func updateIndex( tile: Tile, index: TileIndex) {
        tiles[index.collumn, index.row] = tile
        tile.index = index
    }
    
    private func addShadow(_ tile: Tile?) {
        tile?.layer.shadowColor = UIColor.black.cgColor
        tile?.layer.shadowOpacity = 0.9
        tile?.layer.shadowOffset = .zero
        tile?.layer.shadowRadius = 6
    }
    
    private func removeShadow(_ tile: Tile?) {
        tile?.layer.shadowColor = nil
        tile?.layer.shadowOpacity = 0
        tile?.layer.shadowOffset = .zero
        tile?.layer.shadowRadius = 0
    }
    
    func validateGrid(_ highlight: Bool = true, moved: Bool = false) {
        correctRows = 0
        var shouldShake = true
        
        for i in 0..<5 {
            var currentWord = ""
            for j in 0..<5 {
                let tile = tiles[j, i]
                currentWord += tile.text?.lowercased() ?? ""
            }
            if words.contains(currentWord) {
                if !wordsReady.contains(currentWord) && moved == false {
                    wordsReady.append(currentWord)
                    shouldShake = false
                }
                
                correctRows += 1
                if highlight == true {
                    for j in 0..<5 {
                        let tile = tiles[j, i]
                        tile.lock()
                    }
                }
            }
        }
        print("[Grid5x5] correctRows: \(correctRows)")
        if shouldShake == true && moved == false {
            shake()
        }
        
        if moved == false {
            if correctRows > 0 {
                playHapticSuccess()
            }
            
            delegate?.gridDelegateDidValidate(self, correctRows: correctRows)
        }
    }
    
    private func updateColors() {
        for i in 0..<5 {
            for j in 0..<5 {
                let tile = tiles[j, i]
                let color = colorForRow(i)
                tile.highlightColor = color
            }
        }
    }
    
    func finish() {
        updateColors()
        
        for i in 0..<5 {
            for j in 0..<5 {
                let tile = tiles[j, i]
                tile.lock()
            }
        }
    }
    
    func reset() {
        for i in 0..<5 {
            for j in 0..<5 {
                let tile = tiles[j, i]
                tile.reset()
            }
        }
        wordsReady.removeAll()
    }
    
    private func shake() {
        playHapticError()
        
        for i in 0..<5 {
            var currentWord = ""
            for j in 0..<5 {
                let tile = tiles[j, i]
                currentWord += tile.text?.lowercased() ?? ""
            }
            if !wordsReady.contains(currentWord) {
                for j in 0..<5 {
                    let tile = tiles[j, i]
                    tile.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                }
            }
        }
    }
    
    private func colorForRow(_ row: Int) -> UIColor {
        switch row {
        case 0:
            return .appPalette.yellow
        case 1:
            return .appPalette.purple
        case 2:
            return .appPalette.blue
        case 3:
            return .appPalette.green
        case 4:
            return .appPalette.orange
        default:
            return .appPalette.green
        }
    }
    
    private func prepareSound() {
        guard let url = Bundle.main.url(forResource: "sound", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.ambient, options: .mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        feedbackGenerator.prepare()
    }
    
    private func playSuccess() {
        player?.play()
        playHapticSuccess()
    }
    
    private func playHapticSuccess() {
        feedbackGenerator.notificationOccurred(.success)
    }
    
    private func playHapticError() {
        feedbackGenerator.notificationOccurred(.error)
    }
}

extension Grid5x5: Validatable {
    func validate() {
        validateGrid()
    }
}
