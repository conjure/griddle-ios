//
//  NavigationController.swift
//  Gridle
//
//  Created by Piotr Wilk on 04/02/2022.
//

import UIKit

class NavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        navigationBar.directionalLayoutMargins.leading = 24
        navigationBar.directionalLayoutMargins.trailing = 24

        setDefaultTheme()
    }

    func setDefaultTheme() {
        navigationBar.tintColor = .white
        navigationBar.barStyle = .black
        
        let paragraphStyle = NSMutableParagraphStyle()
        //paragraphStyle.paragraphSpacingBefore = 50
        //paragraphStyle.lineSpacing = 24
        //paragraphStyle.paragraphSpacing = -24
        let fontSize: CGFloat = UIScreen.main.bounds.width <= 320 ? 22 : 28
        navigationBar.titleTextAttributes = [
            .font: UIFont.systemFont(ofSize: fontSize, weight: .bold),
            .foregroundColor: UIColor.white,
            .paragraphStyle: paragraphStyle
        ]
        navigationBar.largeTitleTextAttributes = [
            .font: UIFont.systemFont(ofSize: fontSize, weight: .bold),
            .foregroundColor: UIColor.white,
            .paragraphStyle: paragraphStyle
        ]
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        topViewController?.preferredStatusBarStyle ?? .darkContent
    }
}

