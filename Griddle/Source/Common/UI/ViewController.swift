//
//  ViewController.swift
//  Griddle
//
//  Created by Piotr Wilk on 11/02/2022.
//

import UIKit

class ViewController: UIViewController {
    private var spinnerView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .appPalette.darkGray

        spinnerView.hidesWhenStopped = true
        spinnerView.color = UIColor.black
        view.addSubview(spinnerView)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.bringSubviewToFront(spinnerView)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        spinnerView.center = view.center
    }

    func showSpinner() {
        spinnerView.startAnimating()
    }

    func hideSpinner() {
        spinnerView.stopAnimating()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
