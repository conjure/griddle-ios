//
//  Tile.swift
//  Gridle
//
//  Created by Piotr Wilk on 04/02/2022.
//

import UIKit

class Tile: BaseView {
    private(set) var roundedView = Subviews.roundedView
    private let letterLabel = Subviews.letterLabel
    private var tileInset: CGFloat = 8
    private(set) var isLocked: Bool = false
    
    var basePosition = CGPoint.zero
    var index = TileIndex(0, 0)
    var highlightColor = UIColor.appPalette.green
    var sideColor: UIColor?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(roundedView)
        addSubview(letterLabel)
        
        // size
        let screenWidth = UIScreen.main.bounds.width
        if screenWidth <= 375 {
            tileInset = 6
        } else if screenWidth > 375 && screenWidth <= 414 {
            tileInset = 7
        } else {
            tileInset = 8
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        roundedView.frame = bounds.insetBy(dx: tileInset, dy: tileInset);
        
        letterLabel.frame = bounds
        
        let fontSize = (bounds.width * 0.61).rounded(.down) //roundedView.bounds.width * 0.82
        letterLabel.font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
    }
    
    func snapTo(_ location: CGPoint) {
        UIView.animate(withDuration: 0.25,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 5,
                       options:.curveEaseInOut) {
            self.center = location
        } completion: { success in
            //
        }
    }
    
    func highlight(_ keep: Bool) {
        UIView.animate(withDuration: 0.25, delay: 0, options: []) {
            self.roundedView.backgroundColor = self.highlightColor
        } completion: { success in
            if keep == false {
                UIView.animate(withDuration: 0.25, delay: 0.25, options: []) {
                    self.roundedView.backgroundColor = .appPalette.tileGray
                } completion: { success in
                    //
                }
            }
        }
    }
    
    private func animateLabelWhite() {
        UIView.transition(with: letterLabel,
                          duration: 0.25,
                          options: .transitionCrossDissolve) {
            self.letterLabel.textColor = .white
        } completion: { _ in }
    }
    
    func lock() {
        isLocked = true
        
        UIView.animate(withDuration: 0.25, delay: 0, options: []) {
            self.roundedView.backgroundColor = self.highlightColor
        } completion: { _ in }
        
        UIView.transition(with: letterLabel,
                          duration: 0.25,
                          options: .transitionCrossDissolve) {
            self.letterLabel.textColor = .appPalette.darkGray
        } completion: { _ in }
    }
    
    func reset() {
        if sideColor != nil {
            roundedView.backgroundColor = sideColor
            roundedView.layer.cornerRadius = 0
        } else {
            UIView.animate(withDuration: 0.25) {
                self.roundedView.backgroundColor = .appPalette.tileGray
            }
        }
        animateLabelWhite()
        isLocked = false
    }
    
    var value: String? {
        didSet {
            letterLabel.text = value?.uppercased()
        }
    }
    
    var text: String? {
        get {
            return letterLabel.text
        }
    }
}

private struct Subviews {
    static var roundedView: UIView {
        let view = UIView(frame: .zero)
        //view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.backgroundColor = .appPalette.tileGray
        
        return view
    }
    
    static var letterLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        //label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 42, weight: .semibold)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 1
        
        return label
    }
}
