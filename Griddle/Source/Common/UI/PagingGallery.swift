//
//  PagingGalleryView.swift
//  Griddle
//
//  Created by Piotr Wilk on 17/02/2022.
//

import UIKit

protocol PagingGalleryViewDataSource: AnyObject {
    func pagingGalleryViewNuberOfPages(_ galleryView: PagingGalleryView) -> Int
    func pagingGalleryView(_ galleryView: PagingGalleryView, viewFor index: Int) -> UIView
}

protocol PagingGalleryViewDelegate: AnyObject {
    func pagingGalleryView(_ galleryView: PagingGalleryView, didScrollTo index: Int)
}

class PagingGalleryView: BaseView {
    private let scrollView = Subviews.scrollView
    private let contentView = Subviews.contentView
    private var pages = [UIView]()
    
    weak var dataSource: PagingGalleryViewDataSource?
    weak var delegate: PagingGalleryViewDelegate?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        scrollView.delegate = self
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraints = [
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    func reloadData() {
        pages.forEach({ $0.removeFromSuperview() })
        pages.removeAll()
        
        let pageCount = dataSource?.pagingGalleryViewNuberOfPages(self) ?? 0
        for i in 0..<pageCount {
            if let page = dataSource?.pagingGalleryView(self, viewFor: i) {
                pages.append(page)
            }
        }
        
        let width: CGFloat = UIScreen.main.bounds.width
        var leadingAnchor: NSLayoutXAxisAnchor = contentView.leadingAnchor
        var pageView: UIView?
        
        var constraints = [NSLayoutConstraint]()
        for i in 0..<pages.count {
            let page = pages[i]
            page.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(page)
            
            constraints.append(page.topAnchor.constraint(equalTo: contentView.topAnchor))
            constraints.append(page.leadingAnchor.constraint(equalTo: leadingAnchor))
            constraints.append(page.widthAnchor.constraint(equalToConstant: width))
            constraints.append(page.bottomAnchor.constraint(equalTo: contentView.bottomAnchor))
            
            leadingAnchor = page.trailingAnchor
            pageView = page
        }
        constraints.append(pageView!.trailingAnchor.constraint(equalTo: contentView.trailingAnchor))
        
        NSLayoutConstraint.activate(constraints)
        setNeedsLayout()
    }
}

extension PagingGalleryView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width + 0.5
        if !currentPage.isNaN {
            delegate?.pagingGalleryView(self, didScrollTo: Int(currentPage))
        }
    }
}

private struct Subviews {
    static var scrollView: UIScrollView {
        let view = UIScrollView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isPagingEnabled = true
        view.showsHorizontalScrollIndicator = false
        
        return view
    }
    
    static var contentView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        
        return view
    }
}
