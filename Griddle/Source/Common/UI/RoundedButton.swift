//
//  RoundedButton.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 04/08/2021.
//

import UIKit

class RoundedButton: UIButton {
    static var baseFont = UIFont.systemFont(ofSize: 18, weight: .bold)
    private let disabledColour = UIColor(hex: 0xB2B2B2)

    // MARK: - Initialization

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        setup()
    }

    override required init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    private func setup() {
        layer.cornerRadius = 8

        backgroundColor = .appPalette.cyan
        titleLabel?.font = RoundedButton.baseFont
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.1
        setTitleColor(.black, for: .normal)
        setTitleColor(.black, for: .selected)
        setTitleColor(.white, for: .highlighted)
    }

    /*override var isEnabled: Bool {
        get {
            super.isEnabled
        }
        set {
            super.isEnabled = newValue
            backgroundColor = isEnabled ? .appColor(.green) : disabledColour
            titleLabel?.font = RoundedButton.baseFont
        }
    }*/
}
