//
//  HowGalleryPageFactory.swift
//  Griddle
//
//  Created by Piotr Wilk on 18/02/2022.
//

import Foundation
import UIKit

struct HowGalleryPageFactory {
    
    func createPage(_ index: Int) -> HowGalleryPageView {
        guard index < images.count, let type = HowGalleryPageType(rawValue: index) else { return HowGalleryPageView(.pageOne) }
        
        let image = images[index]
        let imageSide = sideImages[index]
        let text = descriptions[index]
        
        let page = HowGalleryPageView(type)
        page.image = image
        page.imageSide = imageSide
        page.text = text
        
        return page
    }
    
    private var images: [UIImage] {
        [
            UIImage(named: "how-grid-1") ?? UIImage(),
            UIImage(named: "how-grid-2") ?? UIImage(),
            UIImage(named: "how-grid-3") ?? UIImage()
        ]
    }
    
    private var sideImages: [UIImage] {
        [
            UIImage(named: "how-side-1") ?? UIImage(),
            UIImage(named: "how-side-2") ?? UIImage(),
            UIImage()
        ]
    }
    
    private var descriptions: [String] {
        [
            "Slide the letters up and down to create words from left to right.",
            "Find all 5 words to complete the puzzle. Not all words will be correct.",
            "Tap 'check' at any time to see which words you've found. Can you complete the game without checking?"
        ]
    }
}
