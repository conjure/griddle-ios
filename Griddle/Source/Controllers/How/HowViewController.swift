//
//  HowViewController.swift
//  Griddle
//
//  Created by Piotr Wilk on 18/02/2022.
//

import UIKit

class HowViewController: ViewController {
    @IBOutlet private var galleryView: PagingGalleryView!
    @IBOutlet private var pageView: UIPageControl!
    @IBOutlet private var logoImageView: UIImageView!
    
    private let pageFactory = HowGalleryPageFactory()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = UIImage(named: "back-nav-icon")?.withRenderingMode(.alwaysOriginal)
        let barItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(onNavBack))
        navigationItem.leftBarButtonItem = barItem
        
        title = "How to Play"
        
        galleryView.dataSource = self
        galleryView.delegate = self
        galleryView.reloadData()
        
        pageView.isUserInteractionEnabled = false
        if #available(iOS 14.0, *) {
            pageView.preferredIndicatorImage = UIImage(named: "page-dot-other")
        }
        
        logoImageView.image = UIImage(named: "logo-bottom")
        logoImageView.contentMode = .scaleAspectFit
    }
    
    @objc
    func onNavBack() {
        navigationController?.popViewController(animated: true)
    }
}

extension HowViewController: PagingGalleryViewDataSource {
    func pagingGalleryViewNuberOfPages(_ galleryView: PagingGalleryView) -> Int {
        3
    }
    
    func pagingGalleryView(_ galleryView: PagingGalleryView, viewFor index: Int) -> UIView {
        pageFactory.createPage(index)
    }
}

extension HowViewController: PagingGalleryViewDelegate {
    func pagingGalleryView(_ galleryView: PagingGalleryView, didScrollTo index: Int) {
        pageView.currentPage = index
    }
}
