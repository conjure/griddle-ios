//
//  HowGalleryPageView.swift
//  Griddle
//
//  Created by Piotr Wilk on 18/02/2022.
//

import UIKit

enum HowGalleryPageType: Int {
    case pageOne
    case pageTwo
    case pageThree
}

class HowGalleryPageView: BaseView {
    private let imageView = Subviews.imageView
    private let sideImageView = Subviews.sideImageView
    private let textLabel = Subviews.textLabel
    private let pageType: HowGalleryPageType
    
    init(_ type: HowGalleryPageType) {
        self.pageType = type
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(imageView)
        addSubview(sideImageView)
        addSubview(textLabel)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let padding = UIScreen.main.bounds.width * 0.195
        let imageSize = UIScreen.main.bounds.width - padding * 2
        var spaceTop = UIScreen.main.bounds.height * 0.06
        if UIDevice.current.userInterfaceIdiom == .pad {
            spaceTop = 40
        }
        let paddingImage = padding
        var constraints = [
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -spaceTop),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: paddingImage),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -paddingImage),
            imageView.heightAnchor.constraint(equalToConstant: imageSize),
            imageView.widthAnchor.constraint(equalToConstant: imageSize),
            textLabel.centerYAnchor.constraint(equalTo: imageView.bottomAnchor, constant: spaceTop * 1.52),
            textLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding)
        ]
        
        if pageType == .pageOne {
            sideImageView.contentMode = .top
            let spaceTop = padding * 0.71
            let constraintsSide = [
                sideImageView.topAnchor.constraint(equalTo: imageView.topAnchor, constant: spaceTop),
                sideImageView.trailingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: -14),
                sideImageView.heightAnchor.constraint(equalToConstant: 80),
                sideImageView.widthAnchor.constraint(equalToConstant: 23),
            ]
            constraints.append(contentsOf: constraintsSide)
        }
        
        if pageType == .pageTwo {
            sideImageView.contentMode = .bottom
            let spaceBottom = padding * 0.75
            let constraintsSide = [
                sideImageView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -spaceBottom),
                sideImageView.trailingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: -14),
                sideImageView.heightAnchor.constraint(equalToConstant: 23),
                sideImageView.widthAnchor.constraint(equalToConstant: 23),
            ]
            constraints.append(contentsOf: constraintsSide)
        }
        
        NSLayoutConstraint.activate(constraints)
    }
    
    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    var imageSide: UIImage? {
        didSet {
            sideImageView.image = imageSide
        }
    }
    
    var text: String? {
        didSet {
            textLabel.text = text
        }
    }
}

private struct Subviews {
    static var imageView: UIImageView {
        let view = UIImageView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }
    
    static var sideImageView: UIImageView {
        let view = UIImageView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .center
        
        return view
    }
    
    static var textLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        let fontSize: CGFloat = UIScreen.main.bounds.width >= 375 ? 18 : 14
        label.font = UIFont.systemFont(ofSize: fontSize, weight: .regular)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        
        return label
    }
}
