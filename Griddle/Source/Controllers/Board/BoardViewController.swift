//
//  BoardViewController.swift
//  Gridle
//
//  Created by Piotr Wilk on 04/02/2022.
//

import UIKit
import StoreKit

protocol BoardViewControllerDelegate: AnyObject {
    func boardViewControllerDidTapHowToPlay(_ viewController: BoardViewController)
}

class BoardViewController: ViewController, AlertPresentable {
    @IBOutlet private var grid: Grid5x5!
    @IBOutlet private var checkButton: RoundedButton!
    
    weak var delegate: BoardViewControllerDelegate?
    var wordService: WordProvidable!
    var adService: AdProvidable!
    var localStorage: LocalStorage!
    
    private var checkCount: Int     = 0
    private let checkCountMax: Int  = 3
    
    //TODO: Use `LocalStorage` for persistent value
    private var gamesCount: Int     = 0
    
    private struct ButtonTitle {
        static let check = "CHECK"
        static let nextGame = "NEXT GAME"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.setHidesBackButton(true, animated: false)
        
        let image = UIImage(named: "how-nav-icon")?.withRenderingMode(.alwaysOriginal)
        let barItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(onNavHow))
        navigationItem.rightBarButtonItem = barItem
        
        title = "GRIDDLE"
        
        checkButton.setTitle(ButtonTitle.check, for: .normal)
        
        // ads
        adService.delegate = self
        
        // game
        resetGame()
        
        // grid
        grid.delegate = self
        
        //TODO: Remove/uncomment for the AppStore release!
        #if DEBUG
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTripleTap))
        tapRecognizer.numberOfTapsRequired = 3
        view.addGestureRecognizer(tapRecognizer)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(onLongPress))
        longPressRecognizer.minimumPressDuration = 2
        view.addGestureRecognizer(longPressRecognizer)
        #endif
    }
    
    private func resetGame() {
        checkButton.setTitle(ButtonTitle.check, for: .normal)
        view.backgroundColor = .appPalette.darkGray
        
        wordService.randomizeWords(5)
        grid.load(wordService.wordsVertical, solved: wordService.words)
    }
    
    private func finishGame() {
        checkButton.setTitle(ButtonTitle.nextGame, for: .normal)
        grid.finish()
    }
    
    private func handlePostAd() {
        if checkCount == 0 {
            resetGame()
            runOnMainThreadAsyncAfter(1) {
                self.storeGameCount()
            }
        } else if checkCount >= checkCountMax {
            checkCount = 0
        }
    }
    
    @IBAction
    func onCheck() {
        print("[BoardViewController] onCheck checkCount: \(checkCount)")
        if checkButton.title(for: .normal) == ButtonTitle.nextGame {
            checkCount = 0
            presentNextLevelAd()
            return
        }
        
        checkButton.setTitle(ButtonTitle.check, for: .normal)
        grid.validate()
        
        checkCount += 1
        if checkCount == checkCountMax {
            presentSameLevelAd()
            return
        }
    }
    
    @objc
    func onNavHow() {
        delegate?.boardViewControllerDidTapHowToPlay(self)
    }
    
    @objc
    func onTripleTap() {
        resetGame()
    }
    
    @objc
    func onLongPress() {
        presentDebug()
    }
    
    private func storeGameCount() {
        var gamesCompleted: Int = 1
        if var count = localStorage.int(forKey: .gamesCompleted) {
            count += 1
            gamesCompleted = count
            localStorage.setInt(gamesCompleted, forKey: .gamesCompleted)
        } else {
            localStorage.setInt(1, forKey: .gamesCompleted)
        }
        
        if gamesCompleted == 2 || gamesCompleted == 8 {
            if #available(iOS 14.0, *) {
                if let window = UIApplication.shared.activeWindowScene {
                    SKStoreReviewController.requestReview(in: window)
                }
            } else {
                SKStoreReviewController.requestReview()
            }
        }
    }

    private func presentNextLevelAd() {
        showSpinner()
        adService.requestNextLevelAd(self) { error in
            self.hideSpinner()
            if let error = error {
                print("[BoardViewController] presentNextLevelAd error: \(error.localizedDescription)")
                self.handlePostAd()
            }
        }
    }
    
    private func presentSameLevelAd() {
        showSpinner()
        adService.requestSameLevelAd(self) { error in
            self.hideSpinner()
            if let error = error {
                print("[BoardViewController] presentSameLevelAd error: \(error.localizedDescription)")
                self.handlePostAd()
            }
        }
    }
    
    func presentDebug() {
        let debugScreen: BoardDebugViewController = UIStoryboard.instantiateViewController()
        debugScreen.words = wordService.words
        navigationController?.present(debugScreen, animated: true, completion: nil)
    }
}

extension BoardViewController: GridDelegate {
    func gridDelegateDidMove(_ grid: Grid5x5, tile: Tile) {
        if grid.correctRows == 5 {
            finishGame()
        }
    }
    
    func gridDelegateDidValidate(_ grid: Grid5x5, correctRows: Int) {}
}

extension BoardViewController: AdProvidableDelegate {
    func adServiceDidDismissFullScreenContent(_ service: AdProvidable) {
        handlePostAd()
    }
}
