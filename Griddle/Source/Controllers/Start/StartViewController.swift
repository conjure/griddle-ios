//
//  StartViewController.swift
//  Griddle
//
//  Created by Piotr Wilk on 22/02/2022.
//

import UIKit

protocol StartViewControllerDelegate: AnyObject {
    func startViewControllerDidTapStart(_ viewController: StartViewController)
}

class StartViewController: ViewController {
    @IBOutlet private var galleryView: PagingGalleryView!
    @IBOutlet private var pageView: UIPageControl!
    @IBOutlet private var startButton: RoundedButton!
    @IBOutlet private var logoImageView: UIImageView!
    
    weak var delegate: StartViewControllerDelegate?
    private let pageFactory = HowGalleryPageFactory()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "How to Play"
        
        galleryView.dataSource = self
        galleryView.delegate = self
        galleryView.reloadData()
        
        pageView.isUserInteractionEnabled = false
        if #available(iOS 14.0, *) {
            pageView.preferredIndicatorImage = UIImage(named: "page-dot-other")
        }
        
        startButton.backgroundColor = UIColor(hex: 0x414141)
        startButton.setTitleColor(.white, for: .normal)
        startButton.setTitleColor(.white, for: .selected)
        startButton.setTitleColor(.black, for: .highlighted)
        startButton.setTitle("START PLAYING", for: .normal)
        startButton.layer.cornerRadius = 5
        startButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        startButton.layer.shadowColor = UIColor.black.cgColor
        startButton.layer.shadowRadius = 20
        startButton.layer.shadowOpacity = 0.2
        
        logoImageView.image = UIImage(named: "logo-bottom")
        logoImageView.contentMode = .scaleAspectFit
    }
    
    @IBAction
    func onStart() {
        delegate?.startViewControllerDidTapStart(self)
    }
}

extension StartViewController: PagingGalleryViewDataSource {
    func pagingGalleryViewNuberOfPages(_ galleryView: PagingGalleryView) -> Int {
        3
    }
    
    func pagingGalleryView(_ galleryView: PagingGalleryView, viewFor index: Int) -> UIView {
        pageFactory.createPage(index)
    }
}

extension StartViewController: PagingGalleryViewDelegate {
    func pagingGalleryView(_ galleryView: PagingGalleryView, didScrollTo index: Int) {
        pageView.currentPage = index
        
        if index == 2 {
            startButton.layer.borderWidth = 2
            startButton.layer.borderColor = UIColor.appPalette.cyan.cgColor
        } else {
            startButton.layer.borderWidth = 0
            startButton.layer.borderColor = nil
        }
    }
}

