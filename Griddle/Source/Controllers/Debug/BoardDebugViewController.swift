//
//  BoardDebugViewController.swift
//  Gridle
//
//  Created by Piotr Wilk on 04/02/2022.
//

import UIKit

class BoardDebugViewController: UIViewController {
    @IBOutlet private var textView: UITextView!
    var words = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var descriptionStr = "\n"
        for wordStr in words {
            descriptionStr += wordStr
            descriptionStr += "\n"
        }
        
        descriptionStr += "\n\n\n"
        descriptionStr += "Version: " + versionString
        
        textView.text = descriptionStr
    }
    
    var versionString: String {
        var versionString = ""
        if let versionShort = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
           let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            versionString = versionShort + " (\(version))"
        }

        return versionString
    }
}
