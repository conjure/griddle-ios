//
//  AppCoordinator.swift
//  Griddle
//
//  Created by Piotr Wilk on 22/02/2022.
//

import UIKit

class AppCoordinator: Coordinator {
    var parent: Coordinator?
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    var dependencies: AppDependable
    
    required init(_ navigationController: UINavigationController, dependencies: AppDependable) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        if let isFirstAppLaunch = dependencies.localStorage.bool(forKey: .firstAppLaunch), isFirstAppLaunch == false {
            presentBoard()
        } else {
            dependencies.localStorage.setBool(false, forKey: .firstAppLaunch)
            presentStart()
        }
    }
    
    private func presentStart() {
        let startViewController: StartViewController = UIStoryboard.instantiateViewController()
        startViewController.delegate = self
        
        navigationController.pushViewController(startViewController, animated: false)
    }
    
    private func presentBoard() {
        let boardViewController: BoardViewController = UIStoryboard.instantiateViewController()
        boardViewController.wordService = dependencies.wordService
        boardViewController.adService = dependencies.adService
        boardViewController.localStorage = dependencies.localStorage
        boardViewController.delegate = self
        
        navigationController.pushViewController(boardViewController, animated: true)
    }
    
    private func presentHow() {
        let howViewController: HowViewController = UIStoryboard.instantiateViewController()
        
        navigationController.pushViewController(howViewController, animated: true)
    }
}

extension AppCoordinator: StartViewControllerDelegate {
    func startViewControllerDidTapStart(_ viewController: StartViewController) {
        presentBoard()
    }
}

extension AppCoordinator: BoardViewControllerDelegate {
    func boardViewControllerDidTapHowToPlay(_ viewController: BoardViewController) {
        presentHow()
    }
}
