//
//  AppDependencies.swift
//  Griddle
//
//  Created by Piotr Wilk on 22/02/2022.
//

import Foundation

class AppDependencies: AppDependable {
    var wordService: WordProvidable
    var adService: AdProvidable
    var localStorage: LocalStorage
    
    init(_ wordService: WordProvidable, adService: AdProvidable, localStorage: LocalStorage) {
        self.wordService = wordService
        self.adService = adService
        self.localStorage = localStorage
    }
}
